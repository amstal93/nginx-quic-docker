#!/bin/bash

set -ex

export FULL_VERSION=$(curl --silent --fail --location "https://aur.archlinux.org/rpc?v=5&type=info&arg[]=nginx-quic" | jq -r '.results[0].Version')
export VERSION=${FULL_VERSION%-*}

echo "FULL_VERSION=$FULL_VERSION" > $CI_ENV_FILE && echo "VERSION=$VERSION" >> $CI_ENV_FILE

if [[ -f $LAST_VERSION_FILE ]]; then
  export LAST_VERSION=$(<$LAST_VERSION_FILE)
fi
if [[ -n $LAST_VERSION && $LAST_VERSION != $FULL_VERSION ]]; then
  export CI_USE_CACHE=false
fi
params=()
export params

# https://gitlab.com/gitlab-org/gitlab-foss/-/issues/17861#note_19140733
if [[ $CI_USE_CACHE == true ]]; then
  if docker pull "$CI_IMAGE_TAG"; then
    echo "Using existing image $CI_IMAGE_TAG as cache source"
    params+=(--cache-from "$CI_IMAGE_TAG")
  elif docker pull "$CI_REGISTRY_IMAGE:main"; then
    echo "Falling back to $CI_REGISTRY_IMAGE:main as cache source"
    params+=(--cache-from "$CI_REGISTRY_IMAGE:main")
  elif docker pull "$CI_REGISTRY_IMAGE:master"; then
    echo "Falling back to $CI_REGISTRY_IMAGE:master as cache source"
    params+=(--cache-from "$CI_REGISTRY_IMAGE:master")
  else
    echo "No available cache source found, building fresh"
    params+=(--no-cache)
  fi
else
  echo "CI_USE_CACHE is false"
  params+=(--no-cache)
fi

docker build "${params[@]}" -t "${CI_IMAGE_TAG}" .
