FROM archlinux:base-devel

RUN groupadd --gid 101 --system nginx \
    && useradd --uid 101 --gid nginx --system --create-home --home-dir /var/cache/nginx --shell /sbin/nologin nginx
RUN sed -Ei 's/^#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/' /etc/makepkg.conf

RUN set -x \
    && pacman --noconfirm -Sy git \
    && echo "nginx ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/nginx \
    && mkdir -m 777 /aur \
    && su nginx -s /bin/sh -c ' \
        cd /aur \
        && git clone https://aur.archlinux.org/nginx-quic.git \
        && cd nginx-quic \
        && binarypkg=$(makepkg --packagelist | head -1) \
        && makepkg -scr --noconfirm \
        && sudo pacman --noconfirm -U $binarypkg \
    ' \
    && rm -f /etc/sudoers.d/nginx \
    && rm -rf /aur \
    && pacman --noconfirm -Rs git \
    && rm -rf /var/cache/pacman/pkg/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

COPY nginx.conf /etc/nginx/nginx.conf

# Generate self-signed HTTPS certificate on startup to demonstrate QUIC capabilities
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 80
EXPOSE 443/tcp
EXPOSE 443/udp

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]
